package katas

import (
	"testing"
)

func TestWordsToMarks(t *testing.T) {
	if result := WordsToMarks("attitude"); result != 100 {
		t.Fail()
	}
	if result := WordsToMarks("friends"); result != 75 {
		t.Fail()
	}
	if result := WordsToMarks("family"); result != 66 {
		t.Fail()
	}
	if result := WordsToMarks("selfness"); result != 99 {
		t.Fail()
	}
	if result := WordsToMarks("knowledge"); result != 96 {
		t.Fail()
	}
}
