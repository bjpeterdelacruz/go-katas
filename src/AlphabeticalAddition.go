package katas

// AddLetters adds the position value of each letter (a = 1, b = 2, c = 3, etc.) and returns
// another letter that represents the sum modulo 26. If the sum is 26 or the array is empty,
// 'z' will be returned.
func AddLetters(letters []rune) rune {
	if len(letters) == 0 {
		return 'z'
	}
	sum := 0
	for _, letter := range letters {
		sum += int(letter) - 96
	}
	if sum%26 == 0 {
		return 'z'
	}
	return rune(sum%26 + 96)
}
