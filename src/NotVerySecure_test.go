package katas

import (
	"testing"
)

func TestAlphanumeric(t *testing.T) {
	if result := Alphanumeric(""); result {
		t.Fail()
	}
	if result := Alphanumeric("Hello World"); result {
		t.Fail()
	}
	if result := Alphanumeric("Hello_World"); result {
		t.Fail()
	}
	if result := Alphanumeric("!Hello%World?"); result {
		t.Fail()
	}
	if result := Alphanumeric("HelloWorld"); !result {
		t.Fail()
	}

	if result := Alphanumeric(".*?"); result {
		t.Fail()
	}
	if result := Alphanumeric("a"); !result {
		t.Fail()
	}
	if result := Alphanumeric("Mazinkaiser"); !result {
		t.Fail()
	}
	if result := Alphanumeric("hello world_"); result {
		t.Fail()
	}
	if result := Alphanumeric("PassW0rd"); !result {
		t.Fail()
	}
	if result := Alphanumeric("     "); result {
		t.Fail()
	}
	if result := Alphanumeric("\n\t\n"); result {
		t.Fail()
	}
	if result := Alphanumeric("ciao\n$$_"); result {
		t.Fail()
	}
	if result := Alphanumeric("__ * __"); result {
		t.Fail()
	}
	if result := Alphanumeric("&)))((("); result {
		t.Fail()
	}
	if result := Alphanumeric("43534h56jmTHHF3k"); !result {
		t.Fail()
	}
}
