package katas

import (
	"strings"
)

// BandNameGenerator returns a new name for a band given the following rules: if the first and last
// letters of the given word are equal, then the word is repeated twice, the first letter of the
// repeated word is dropped, the two words are concatenated together, and the first letter of the
// new word is capitalized. Otherwise, the first letter of the word is capitalized, and "The " is
// attached to the front of the word.
func BandNameGenerator(word string) string {
	lowercase := strings.ToLower(word)
	newWord := strings.Title(lowercase)
	if lowercase[0] == lowercase[len(word)-1] {
		return newWord[:len(word)-1] + lowercase
	}
	return "The " + newWord
}
