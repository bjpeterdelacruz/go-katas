package katas

// HasUniqueChar returns true if each character in the given string appears only once, or false
// otherwise.
func HasUniqueChar(str string) bool {
	var uniqueCharacters = make(map[rune]bool)
	for _, character := range str {
		if uniqueCharacters[character] {
			return false
		}
		uniqueCharacters[character] = true
	}
	return true
}
