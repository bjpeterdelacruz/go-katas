package katas

// StringEndsWith returns true if the given string ends with the given suffix.
func StringEndsWith(str, ending string) bool {
	if len(str) < len(ending) {
		return false
	}
	strLastIndex := len(str) - 1
	endingLastIndex := len(ending) - 1
	for {
		if endingLastIndex < 0 {
			return true
		}
		if str[strLastIndex] != ending[endingLastIndex] {
			return false
		}
		strLastIndex--
		endingLastIndex--
	}
}
