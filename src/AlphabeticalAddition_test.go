package katas

import (
	"testing"
)

func TestAddLetters(t *testing.T) {
	if AddLetters([]rune{'a', 'b', 'c'}) != 'f' {
		t.Fail()
	}
	if AddLetters([]rune{'a', 'b'}) != 'c' {
		t.Fail()
	}
	if AddLetters([]rune{'z'}) != 'z' {
		t.Fail()
	}
	if AddLetters([]rune{'z', 'a'}) != 'a' {
		t.Fail()
	}
	if AddLetters([]rune{'y', 'c', 'b'}) != 'd' {
		t.Fail()
	}
	if AddLetters([]rune{}) != 'z' {
		t.Fail()
	}
}
