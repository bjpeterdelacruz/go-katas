package katas

import (
	"testing"
)

func TestBandNameGenerator(t *testing.T) {
	if BandNameGenerator("alaska") != "Alaskalaska" {
		t.Fail()
	}
	if BandNameGenerator("knife") != "The Knife" {
		t.Fail()
	}
	if BandNameGenerator("tart") != "Tartart" {
		t.Fail()
	}
	if BandNameGenerator("sandles") != "Sandlesandles" {
		t.Fail()
	}
	if BandNameGenerator("bed") != "The Bed" {
		t.Fail()
	}
}
