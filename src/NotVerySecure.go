package katas

import (
	"regexp"
)

// Alphanumeric returns true if and only if the given string has a length and contains only numbers
// and alphabetic characters.
func Alphanumeric(str string) bool {
	regex := regexp.MustCompile(`[^0-9a-zA-Z]+`)
	return len(str) > 0 && !regex.MatchString(str)
}
