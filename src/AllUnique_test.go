package katas

import (
	"testing"
)

func TestHasUniqueChar(t *testing.T) {
	if result := HasUniqueChar("  nAa"); result {
		t.Fail()
	}
	if result := HasUniqueChar("abcde"); !result {
		t.Fail()
	}
	if result := HasUniqueChar("++-"); result {
		t.Fail()
	}
	if result := HasUniqueChar("AaBbC"); !result {
		t.Fail()
	}
}
