package katas

import (
	"testing"
)

func TestStringEndsWith(t *testing.T) {
	if result := StringEndsWith("", ""); !result {
		t.Fail()
	}
	if result := StringEndsWith(" ", ""); !result {
		t.Fail()
	}
	if result := StringEndsWith("abc", "c"); !result {
		t.Fail()
	}
	if result := StringEndsWith("banana", "ana"); !result {
		t.Fail()
	}
	if result := StringEndsWith("a", "z"); result {
		t.Fail()
	}
	if result := StringEndsWith("", "t"); result {
		t.Fail()
	}
	if result := StringEndsWith("$a = $b + 1", "+1"); result {
		t.Fail()
	}
	if result := StringEndsWith("    ", "   "); !result {
		t.Fail()
	}
	if result := StringEndsWith("1oo", "100"); result {
		t.Fail()
	}
}
