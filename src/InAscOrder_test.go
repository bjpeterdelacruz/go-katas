package katas

import (
	"testing"
)

func TestInAscOrder(t *testing.T) {
	if result := InAscOrder([]int{1, 2, 4, 7, 19}); !result {
		t.Fail()
	}
	if result := InAscOrder([]int{1, 2, 3, 4, 5}); !result {
		t.Fail()
	}
	if result := InAscOrder([]int{1, 6, 10, 18, 2, 4, 20}); result {
		t.Fail()
	}
	if result := InAscOrder([]int{9, 8, 7, 6, 5, 4, 3, 2, 1}); result {
		t.Fail()
	}
	if result := InAscOrder([]int{9}); !result {
		t.Fail()
	}
}
