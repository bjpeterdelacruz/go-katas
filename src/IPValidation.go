package katas

import (
	"strconv"
	"strings"
)

// IsValidIP returns true if the given IPv4 address string is valid, or false otherwise.
func IsValidIP(ip string) bool {
	var octets = strings.Split(ip, ".")
	if len(octets) != 4 {
		return false
	}
	for _, octet := range octets {
		if octet[0] == '0' && len(octet) > 1 {
			return false
		}
		result, err := strconv.Atoi(octet)
		if err != nil {
			return false
		}
		if result < 0 || result > 255 {
			return false
		}
	}
	return true
}
