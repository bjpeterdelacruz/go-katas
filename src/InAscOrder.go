package katas

// InAscOrder returns true if each successive integer in the given array is equal to or greater
// than the previous integer.
func InAscOrder(numbers []int) bool {
	next := 1
	current := 0
	for {
		if next == len(numbers) {
			return true
		}
		if numbers[current] > numbers[next] {
			return false
		}
		next++
		current++
	}
}
