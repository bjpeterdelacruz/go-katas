package katas

import (
	"strings"
)

// WordsToMarks sums the position values of each letter (a = 1, b = 2, c = 3, etc.) in the given
// string.
func WordsToMarks(s string) int {
	sum := 0
	for _, character := range strings.ToLower(s) {
		sum += int(character) - 96
	}
	return sum
}
