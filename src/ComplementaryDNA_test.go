package katas

import (
	"testing"
)

func TestDNAStrand(t *testing.T) {
	if DNAStrand("AAAA") != "TTTT" {
		t.Fail()
	}
	if DNAStrand("ATTGC") != "TAACG" {
		t.Fail()
	}
	if DNAStrand("GTAT") != "CATA" {
		t.Fail()
	}
}
