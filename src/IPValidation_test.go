package katas

import (
	"testing"
)

func TestIsValidIPInvalid(t *testing.T) {
	if result := IsValidIP("1.2.3"); result {
		t.Fail()
	}
	if result := IsValidIP("1.2.03.4"); result {
		t.Fail()
	}
	if result := IsValidIP("1.2.abc.4"); result {
		t.Fail()
	}
	if result := IsValidIP("1.2.-3.4"); result {
		t.Fail()
	}
	if result := IsValidIP("1.2.256.4"); result {
		t.Fail()
	}
	if result := IsValidIP(""); result {
		t.Fail()
	}
	if result := IsValidIP("abc.def.ghi.jkl"); result {
		t.Fail()
	}
	if result := IsValidIP("123.456.789.0"); result {
		t.Fail()
	}
	if result := IsValidIP("12.34.56"); result {
		t.Fail()
	}
	if result := IsValidIP("12.34.56 .1"); result {
		t.Fail()
	}
	if result := IsValidIP("12.34.56.-1"); result {
		t.Fail()
	}
	if result := IsValidIP("123.045.067.089"); result {
		t.Fail()
	}
	if result := IsValidIP("192.168.1.300"); result {
		t.Fail()
	}
}

func TestIsValidIPValid(t *testing.T) {
	if result := IsValidIP("1.2.3.4"); !result {
		t.Fail()
	}
	if result := IsValidIP("12.255.56.1"); !result {
		t.Fail()
	}
	if result := IsValidIP("127.1.1.0"); !result {
		t.Fail()
	}
	if result := IsValidIP("0.0.0.0"); !result {
		t.Fail()
	}
	if result := IsValidIP("0.34.82.53"); !result {
		t.Fail()
	}
}
