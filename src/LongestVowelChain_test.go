package katas

import (
	"testing"
)

func TestLongestVowelChain(t *testing.T) {
	if result := LongestVowelChain("codewarriors"); result != 2 {
		t.Fail()
	}
	if result := LongestVowelChain("suoidea"); result != 3 {
		t.Fail()
	}
	if result := LongestVowelChain("ultrarevolutionariees"); result != 3 {
		t.Fail()
	}
	if result := LongestVowelChain("strengthlessnesses"); result != 1 {
		t.Fail()
	}
	if result := LongestVowelChain("cuboideonavicuare"); result != 2 {
		t.Fail()
	}
	if result := LongestVowelChain("chrononhotonthuooaos"); result != 5 {
		t.Fail()
	}
	if result := LongestVowelChain("iiihoovaeaaaoougjyaw"); result != 8 {
		t.Fail()
	}
}
