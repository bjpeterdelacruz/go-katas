package katas

import (
	"strings"
)

// LongestVowelChain returns the length of the lowest substring that contains only vowels.
func LongestVowelChain(s string) int {
	vowels := "aeiou"
	max := 0
	count := 0
	for _, character := range s {
		vowelIdx := strings.Index(vowels, string(character))
		if vowelIdx > -1 {
			count++
		} else {
			count = 0
		}
		if count > max {
			max = count
		}
	}
	return max
}
