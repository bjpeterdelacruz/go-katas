package katas

// DNAStrand returns the other complementary side of the given DNA strand.
func DNAStrand(dna string) string {
	complementary := ""
	for _, character := range dna {
		switch character {
		case 'A':
			complementary += "T"
		case 'T':
			complementary += "A"
		case 'C':
			complementary += "G"
		case 'G':
			complementary += "C"
		}
	}
	return complementary
}
